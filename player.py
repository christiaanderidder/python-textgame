#The Player class
class Player(object):

    # Constructor
    def __init__(self, x, y):
        self.x = x
        self.y = y

    # Update the object's properties
    def update(self):
        # TODO: Handle input here and move the player accordingly
        self.move(1,1)
        
    # Draw the object
    def draw(self): 
        print('*', end="")

    def move(self, dx, dy):
        self.x += dx
        self.y += dy
