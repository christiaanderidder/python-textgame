#The Room class
class Room(object):

    # Constructor
    def __init__(self, width, height, player):
        self.width = width
        self.height = height
        self.player = player
        self.tiles = [[' ' for x in range(self.width)] for x in range(self.height)] 

    # Update the object's properties
    def update(self):
        self.player.update()
     
    # Draw the object
    def draw(self): 
        print('▒' * (self.width + 2)) # Top border
        
        for i, row in enumerate(self.tiles):
            print('▒', end="") # Left border
            
            for j, col in enumerate(row):
                if(i == self.player.y and j == self.player.x):
                    self.player.draw()
                else:
                    print(col, end="")

            print('▒') # Right border    
        
        print('▒' * (self.width + 2)) # Bottom border
            
