# Python imports
import time
import os
import sys

# Windows specific
import msvcrt

# Game imports
from player import Player
from room import Room

# Initialize
is_running = True
player = Player(0, 0)
room = Room(10, 10, player)

ESC = 27 # Keycode for the escape key

# The entry point
def main():
    # Main loop, keeps running until is_running is set to False
    while is_running:
        update()

# The main update function
def update():
    key = None
    
    if(msvcrt.kbhit()): # A keypress is waiting
        key = ord(msvcrt.getch()) # Convert the input char to a keycode

    # If escape was pressed, stop the program
    if(key == ESC):
        clear()
        print("Thanks for playing!")
        sys.stdout.flush()
        # In python writing can't be done without the global keyword
        global is_running
        is_running = False
        
    
    room.update() # Update the logic
    clear() # Clear the canvas
    room.draw() # Draw the result
    
    time.sleep(1) # 1 FPS w00t

# A quick trick to clear the console on both windows and *nix
def clear():
    os.system(['clear','cls'][os.name == 'nt'])

# Run the program by calling the entry point
main()
